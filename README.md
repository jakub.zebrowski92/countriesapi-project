# CountriesAPI Project

A project where using [RestAPI](https://restcountries.eu/#rest-countries)
 plus React and other front libraries you can get basic information about countries, population, languages and etc.
## Installation

Use the package manager [npm](https://nodejs.org/en/download/) to install.

```bash
npm install
```

## Usage

```bash
npm start
```

## License
[MIT](https://choosealicense.com/licenses/mit/)