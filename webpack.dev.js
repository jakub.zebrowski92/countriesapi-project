const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'development',
  devtool: "source-map",
  devServer: {
    contentBase: "./src",
    hot: true,
    port: 3000,
    historyApiFallback: {
      index: './src/index.html',
    },
  },
});