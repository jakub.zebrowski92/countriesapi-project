import PropTypes from 'prop-types';
import React from 'react';
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';

export default class ItemsPerPage extends React.Component {
  static propTypes = {
    changePageSize: PropTypes.func.isRequired,
    pageSize: PropTypes.number.isRequired,
    totalDataLength: PropTypes.number.isRequired
  };

  state = {
    dropdownOpen: false
  };

  toggleDropdown = () =>
    this.setState(prevState => ({ dropdownOpen: !prevState.dropdownOpen }));

  createDropdownList = () => {
    const { changePageSize, pageSize, totalDataLength } = this.props;
    const dropDownList = [
      <DropdownItem
        key='all'
        active={pageSize === totalDataLength}
        onClick={() => changePageSize(totalDataLength)}
      >
        all
      </DropdownItem>
    ];
    for (let i = 5; i <= 50; i += 5) {
      dropDownList.push(
        <DropdownItem
          key={i}
          active={pageSize === i}
          onClick={() => changePageSize(i)}
        >
          {i}
        </DropdownItem>
      );
    }
    return dropDownList;
  };

  render() {
    const { dropdownOpen } = this.state;
    const { pageSize, totalDataLength } = this.props;
    return (
      <Dropdown isOpen={dropdownOpen} toggle={this.toggleDropdown}>
        <DropdownToggle caret>
          Items per page: {pageSize === totalDataLength ? 'all' : pageSize}
        </DropdownToggle>
        <DropdownMenu>{this.createDropdownList()}</DropdownMenu>
      </Dropdown>
    );
  }
}
