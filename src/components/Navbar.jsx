import { Navbar, NavbarBrand } from 'reactstrap';
import React from 'react';

const CustomNavbar = () => (
  <div>
    <Navbar color='dark' expand='md'>
      <NavbarBrand href='/'>CountriesAPI Project</NavbarBrand>
    </Navbar>
  </div>
);

export default CustomNavbar;
