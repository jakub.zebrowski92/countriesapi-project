import PropTypes from 'prop-types';
import React from 'react';
import { Badge, Button } from 'reactstrap';

const AdditionalInfoBadge = props => {
  const { text, badgeText } = props;
  return (
    <div className='additionalInfo'>
      <Button color='secondary' outline>
        {text} <Badge color='secondary'>{badgeText}</Badge>
      </Button>
    </div>
  );
};

export default AdditionalInfoBadge;

AdditionalInfoBadge.propTypes = {
  text: PropTypes.string.isRequired,
  badgeText: PropTypes.string.isRequired
};
