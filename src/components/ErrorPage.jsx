import React from 'react';

const ErrorPage = () => {
  return (
    <div id='errorPage'>
      <h3>Something went wrong!</h3>
      <p>
        Please refresh the page and try again or check the internet connection.
        If this will not help please
        <a href='mailto:example@example.eu'> contact us</a>.
      </p>
    </div>
  );
};

export default ErrorPage;
