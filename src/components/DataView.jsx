import PropTypes from 'prop-types';
import React from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';
import Table from './Table.jsx';

export default class DataView extends React.Component {
  static propTypes = {
    averagePopulation: PropTypes.number.isRequired,
    countries: PropTypes.arrayOf(PropTypes.object).isRequired,
    languages: PropTypes.arrayOf(PropTypes.object).isRequired,
    smallestBiggestCountry: PropTypes.shape({
      biggest: PropTypes.shape({
        country: PropTypes.string,
        area: PropTypes.number
      }),
      smallest: PropTypes.shape({
        country: PropTypes.string,
        area: PropTypes.number
      })
    }).isRequired
  };

  state = {
    currentView: 'countries'
  };

  toggle = tab => {
    const { currentView } = this.state;
    if (currentView !== tab) this.setState({ currentView: tab });
  };

  render() {
    const { currentView } = this.state;
    const {
      countries,
      languages,
      averagePopulation,
      smallestBiggestCountry
    } = this.props;
    return (
      <div id='customContainer'>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: currentView === 'countries' })}
              onClick={() => this.toggle('countries')}
            >
              Countries
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: currentView === 'languages' })}
              onClick={() => this.toggle('languages')}
            >
              Languages
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={currentView}>
          <TabPane tabId='countries'>
            <Table
              countries
              data={countries}
              averagePopulation={averagePopulation}
              smallestBiggestCountry={smallestBiggestCountry}
            />
          </TabPane>
          <TabPane tabId='languages'>
            <Table languages data={languages} />
          </TabPane>
        </TabContent>
      </div>
    );
  }
}
