import React from 'react';
import fetchCountries from '../services/api/fetcher';
import LoadingCircle from './Loader.jsx';
import Navbar from './Navbar.jsx';
import DataView from './DataView.jsx';
import ErrorPage from './ErrorPage.jsx';

export default class App extends React.Component {
  state = {
    languages: null,
    countries: null,
    averagePopulation: null,
    smallestBiggestCountry: null,
    appStatus: 'loading'
  };

  componentDidMount() {
    fetchCountries()
      .then(result => {
        const [
          countries,
          languages,
          averagePopulation,
          smallestBiggestCountry
        ] = result;
        this.setState({
          countries,
          languages,
          averagePopulation,
          smallestBiggestCountry,
          appStatus: 'dataView'
        });
      })
      .catch(error => this.setState({ appStatus: 'error' }));
  }

  render() {
    const {
      appStatus,
      countries,
      languages,
      averagePopulation,
      smallestBiggestCountry
    } = this.state;
    return (
      <>
        <Navbar />
        {appStatus === 'loading' && <LoadingCircle />}
        {appStatus === 'dataView' && (
          <DataView
            languages={languages}
            countries={countries}
            averagePopulation={averagePopulation}
            smallestBiggestCountry={smallestBiggestCountry}
          />
        )}
        {appStatus === 'error' && <ErrorPage />}
      </>
    );
  }
}
