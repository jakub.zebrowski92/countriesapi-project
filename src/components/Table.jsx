import PropTypes from 'prop-types';
import React from 'react';
import { Table } from 'reactstrap';
import Pagination from './Pagination.jsx';
import ItemsPerPage from './ItemsPerPage.jsx';
import AdditionalInfoBadge from './AdditionalInfoBadge.jsx';

const sortKeys = {
  Name: 'name',
  Region: 'region',
  'Area (mi\u00b2)': 'area',
  'Population (M)': 'population',
  'Total population': 'populationTotal',
  Languages: 'language'
};

export default class CustomTable extends React.Component {
  static propTypes = {
    averagePopulation: PropTypes.number,
    countries: PropTypes.bool,
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    languages: PropTypes.bool,
    smallestBiggestCountry: PropTypes.shape({
      biggest: PropTypes.shape({
        country: PropTypes.string,
        area: PropTypes.number
      }),
      smallest: PropTypes.shape({
        country: PropTypes.string,
        area: PropTypes.number
      })
    })
  };

  static defaultProps = {
    averagePopulation: 0,
    countries: false,
    languages: false,
    smallestBiggestCountry: {}
  };

  state = {
    page: 1,
    pageSize: 10,
    orderBy: ''
  };

  makeHeaders = () => {
    const { countries, languages } = this.props;
    let header = [];
    if (countries) {
      header = [
        '#',
        'Flag',
        'Name',
        'Region',
        'Area (mi\u00b2)',
        'Population (M)'
      ];
    } else if (languages) {
      header = ['#', 'Languages', 'List of countries', 'Total population'];
    }
    return header;
  };

  makeRows = () => {
    const { countries, languages, data } = this.props;
    const { page, pageSize } = this.state;
    const { orderBy } = this.state;
    let paginatedData;
    if (orderBy) {
      paginatedData = this.dataPerPage(this.sortData(data));
    } else paginatedData = this.dataPerPage(data);
    if (countries) {
      return paginatedData.map((row, ix) => (
        <tr
          style={{ animation: `fadeIn 1s` }}
          key={row.name}
          className='countriesRow'
        >
          <td>{page * pageSize - (pageSize - (ix + 1))}</td>
          <td>
            <img src={row.flag} alt={`${row.name}_flag`} />
          </td>
          <td>{row.name}</td>
          <td>{row.region}</td>
          <td>{row.area}</td>
          <td>{row.population}</td>
        </tr>
      ));
    }
    if (languages) {
      return paginatedData.map((row, ix) => {
        return (
          <tr
            style={{ animation: `fadeIn 1s` }}
            key={row.language}
            className='languagesRow'
          >
            <td>{page * pageSize - (pageSize - (ix + 1))}</td>
            <td>{row.language}</td>
            <td>
              <ul>
                {row.countries.map(country => (
                  <li key={country}>{country}</li>
                ))}
              </ul>
            </td>
            <td>{row.populationTotal}</td>
          </tr>
        );
      });
    }
  };

  assignToSort = header => {
    const { orderBy } = this.state;
    const valFromHeader = sortKeys[header];
    if (orderBy) {
      if (orderBy.hasOwnProperty(valFromHeader)) {
        if (orderBy[valFromHeader] === 'asc')
          this.setState({ orderBy: { [valFromHeader]: 'desc' } });
        else this.setState({ orderBy: '' });
      } else {
        this.setState({ orderBy: { [valFromHeader]: 'asc' } });
      }
    } else this.setState({ orderBy: { [valFromHeader]: 'asc' } });
  };

  sortData = data => {
    const { orderBy } = this.state;
    const key = Object.keys(orderBy)[0];
    return Array.from(data).sort((a, b) => {
      if (key === 'area' || key === 'population' || key === 'populationTotal') {
        const aVal = a[key] === 'N/A' ? 0 : a[key];
        const bVal = b[key] === 'N/A' ? 0 : b[key];
        if (orderBy[key] === 'asc') {
          return aVal - bVal;
        }
        return bVal - aVal;
      }
      if (orderBy[key] === 'asc') {
        return a[key].toLowerCase() > b[key].toLowerCase() ? 1 : -1;
      }
      return a[key].toLowerCase() < b[key].toLowerCase() ? 1 : -1;
    });
  };

  dataPerPage = data => {
    const { page, pageSize } = this.state;
    return data.slice((page - 1) * pageSize, page * pageSize);
  };

  changePageSize = size => {
    const { data } = this.props;
    const { page } = this.state;
    if (Math.ceil(data.length / size) < page)
      this.setState({ pageSize: size, page: 1 });
    else this.setState({ pageSize: size });
  };

  changePage = page => this.setState({ page });

  render() {
    const {
      data,
      countries,
      averagePopulation,
      smallestBiggestCountry
    } = this.props;
    const { page, pageSize, orderBy } = this.state;
    return (
      <>
        <ItemsPerPage
          changePageSize={this.changePageSize}
          pageSize={pageSize}
          totalDataLength={data.length}
        />
        {countries && (
          <>
            <AdditionalInfoBadge
              text='Average population:'
              badgeText={`${averagePopulation} (M)`}
            />
            <AdditionalInfoBadge
              text={`Smallest country: ${
                smallestBiggestCountry.smallest.country
              }`}
              badgeText={`${smallestBiggestCountry.smallest.area} mi\u00b2`}
            />
            <AdditionalInfoBadge
              text={`Biggest country: ${
                smallestBiggestCountry.biggest.country
              }:`}
              badgeText={`${smallestBiggestCountry.biggest.area} mi\u00b2`}
            />
          </>
        )}
        <Table striped bordered responsive>
          <thead>
            <tr>
              {this.makeHeaders().map(header => (
                <th
                  className={
                    sortKeys[header]
                      ? `clickableHeader ${orderBy[sortKeys[header]]}`
                      : ''
                  }
                  onClick={() =>
                    sortKeys[header] ? this.assignToSort(header) : null
                  }
                  key={header}
                >
                  {header}
                </th>
              ))}
            </tr>
          </thead>
          <tbody>{this.makeRows()}</tbody>
        </Table>
        <Pagination
          totalDataLength={data.length}
          page={page}
          pageSize={pageSize}
          changePage={this.changePage}
        />
      </>
    );
  }
}
