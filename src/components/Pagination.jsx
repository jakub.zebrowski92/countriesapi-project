import PropTypes from 'prop-types';
import React from 'react';
import { Pagination } from 'reactstrap';
import createPaginationItems from '../services/utils/paginationLogic';

const CustomPagination = props => {
  const { page, totalDataLength, pageSize, changePage } = props;
  const totalPages = Math.ceil(totalDataLength / pageSize);
  return (
    <Pagination>
      {createPaginationItems(page, totalPages, changePage)}
    </Pagination>
  );
};

export default CustomPagination;

CustomPagination.propTypes = {
  changePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  pageSize: PropTypes.number.isRequired,
  totalDataLength: PropTypes.number.isRequired
};
