import { PaginationItem, PaginationLink } from 'reactstrap';
import React, { Fragment } from 'react';

const createPaginationItems = (page, totalPages, changePage) => {
  const arrayOfPaginationElems = [];
  if (totalPages > 7) {
    arrayOfPaginationElems.push(
      <PaginationItem active={page === 1} key='1'>
        <PaginationLink onClick={() => changePage(1)}>1</PaginationLink>
      </PaginationItem>
    );
    if (page > 3 && page < totalPages - 3) {
      arrayOfPaginationElems.push(
        <Fragment key={`pageRange${page - 1} ${page + 1}`}>
          <PaginationItem key='prev'>
            <PaginationLink onClick={() => changePage(page - 1)} previous />
          </PaginationItem>
          <PaginationItem key={page - 1}>
            <PaginationLink onClick={() => changePage(page - 1)}>
              {page - 1}
            </PaginationLink>
          </PaginationItem>
          <PaginationItem active key={page}>
            <PaginationLink onClick={() => changePage(page)}>
              {page}
            </PaginationLink>
          </PaginationItem>
          <PaginationItem key={page + 1}>
            <PaginationLink onClick={() => changePage(page + 1)}>
              {page + 1}
            </PaginationLink>
          </PaginationItem>
          <PaginationItem key='next'>
            <PaginationLink onClick={() => changePage(page + 1)} next />
          </PaginationItem>
        </Fragment>
      );
    } else if (page > totalPages - 4) {
      arrayOfPaginationElems.push(
        <PaginationItem key='prev'>
          <PaginationLink onClick={() => changePage(page - 1)} previous />
        </PaginationItem>
      );
      for (let i = totalPages - 4; i <= totalPages - 1; i++) {
        arrayOfPaginationElems.push(
          <PaginationItem active={page === i} key={i}>
            <PaginationLink onClick={() => changePage(i)}>{i}</PaginationLink>
          </PaginationItem>
        );
      }
    } else {
      for (let i = 2; i <= 5; i++) {
        arrayOfPaginationElems.push(
          <PaginationItem active={page === i} key={i}>
            <PaginationLink onClick={() => changePage(i)}>{i}</PaginationLink>
          </PaginationItem>
        );
      }
      arrayOfPaginationElems.push(
        <PaginationItem key='next'>
          <PaginationLink onClick={() => changePage(page + 1)} next />
        </PaginationItem>
      );
    }
    arrayOfPaginationElems.push(
      <PaginationItem active={page === totalPages} key={totalPages}>
        <PaginationLink onClick={() => changePage(totalPages)}>
          {totalPages}
        </PaginationLink>
      </PaginationItem>
    );
    return arrayOfPaginationElems;
  }
  for (let i = 1; i <= totalPages; i++) {
    arrayOfPaginationElems.push(
      <PaginationItem active={page === i} key={i}>
        <PaginationLink onClick={() => changePage(i)}>{i}</PaginationLink>
      </PaginationItem>
    );
  }
  return (
    <>
      <PaginationItem key='prev'>
        <PaginationLink
          disabled={page === 1}
          active={page === 1}
          onClick={() => (page === 1 ? null : changePage(page - 1))}
          previous
        />
      </PaginationItem>
      {arrayOfPaginationElems}
      <PaginationItem key='next'>
        <PaginationLink
          active={page === totalPages}
          disabled={page === totalPages}
          onClick={() => (page === totalPages ? null : changePage(page + 1))}
          next
        />
      </PaginationItem>
    </>
  );
};

export default createPaginationItems;
