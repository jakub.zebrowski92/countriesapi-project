import fetchAndPrepareData from '../api/fetcher';

function promisedFunction(promiseFetch) {
  return new Promise(resolve => {
    promiseFetch().then(data => resolve(data));
  });
}

describe('calls fetchAndPrepareData and returns proper array of values', () => {
  let promiseFetch;
  beforeAll(() => {
    promiseFetch = fetchAndPrepareData;
  });
  test('it returns array', () => {
    return promisedFunction(promiseFetch).then(data => {
      expect(Array.isArray(data)).toBe(true);
    });
  });
  test('array contains 4 results', () => {
    return promisedFunction(promiseFetch).then(data => {
      expect(data.length).toBe(4);
    });
  });
  test('it returns array of 250 countries', () => {
    return promisedFunction(promiseFetch).then(data => {
      const [
        countries,
        languages,
        avgPopulation,
        smallestBiggestCountry
      ] = data;
      expect(Array.isArray(countries)).toBe(true);
      expect(countries.length).toBe(250);
    });
  });
  test('it returns array of 112 languages', () => {
    return promisedFunction(promiseFetch).then(data => {
      const [
        countries,
        languages,
        avgPopulation,
        smallestBiggestCountry
      ] = data;
      expect(Array.isArray(languages)).toBe(true);
      expect(languages.length).toBe(112);
    });
  });
  test('it returns avgPopulation which is a 29.4', () => {
    return promisedFunction(promiseFetch).then(data => {
      const [
        countries,
        languages,
        avgPopulation,
        smallestBiggestCountry
      ] = data;
      expect(avgPopulation).toEqual(29.4);
    });
  });
  test('it returns smallestBiggestCountry as object', () => {
    return promisedFunction(promiseFetch).then(data => {
      const [
        countries,
        languages,
        avgPopulation,
        smallestBiggestCountry
      ] = data;
      expect(typeof smallestBiggestCountry).toBe('object');
    });
  });
  test('smallestBiggestCountry contains objects of data', () => {
    return promisedFunction(promiseFetch).then(data => {
      const [
        countries,
        languages,
        avgPopulation,
        smallestBiggestCountry
      ] = data;
      expect(Object.keys(smallestBiggestCountry).length).toBe(2);
      expect(Object.keys(smallestBiggestCountry)[0]).toBe('biggest');
      expect(Object.keys(smallestBiggestCountry)[1]).toBe('smallest');
    });
  });
});
