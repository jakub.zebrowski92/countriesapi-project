import React from 'react';
import renderer from 'react-test-renderer';
import Navbar from '../../components/Navbar.jsx';

describe('NavbarComponent', () => {
  test('snapshot renders', () => {
    const component = renderer.create(<Navbar />);
    let newNavbar = component.toJSON();
    expect(newNavbar).toMatchSnapshot();
  });
});
