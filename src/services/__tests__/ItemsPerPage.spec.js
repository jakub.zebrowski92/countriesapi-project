import React from 'react';
import renderer from 'react-test-renderer';
import ItemsPerPage from '../../components/ItemsPerPage.jsx';

describe('ItemsPerPageComponent', () => {
  test('snapshot renders', () => {
    const component = renderer.create(
      <ItemsPerPage
        changePageSize={() => {}}
        pageSize={10}
        totalDataLength={100}
      />
    );
    let newItemsPerPage = component.toJSON();
    expect(newItemsPerPage).toMatchSnapshot();
  });
});
