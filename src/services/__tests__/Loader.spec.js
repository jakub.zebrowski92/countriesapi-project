import React from 'react';
import renderer from 'react-test-renderer';
import Loader from '../../components/Loader.jsx';

describe('LoaderComponent', () => {
  test('snapshot renders', () => {
    const component = renderer.create(<Loader />);
    let newLoader = component.toJSON();
    expect(newLoader).toMatchSnapshot();
  });
});
