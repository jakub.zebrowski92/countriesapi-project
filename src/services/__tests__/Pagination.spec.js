import React from 'react';
import renderer from 'react-test-renderer';
import Pagination from '../../components/Pagination.jsx';

describe('PaginationComponent', () => {
  test('snapshot renders', () => {
    const component = renderer.create(
      <Pagination
        page={1}
        totalDataLength={100}
        pageSize={10}
        changePage={() => {}}
      />
    );
    let newPagination = component.toJSON();
    expect(newPagination).toMatchSnapshot();
  });
});
