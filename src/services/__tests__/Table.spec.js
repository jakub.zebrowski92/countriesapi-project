import React from 'react';
import renderer from 'react-test-renderer';
import Table from '../../components/Table.jsx';

const smallestBiggestCountry = {
  biggest: {
    country: 'Russia',
    area: 2111
  },
  smallest: {
    country: 'Peru',
    area: 11
  }
};

const testData = [
  { name: 'USA', area: 1111, population: 1545, flag: '', region: 'Americas' },
  { name: 'Norway', area: 1131, population: 15, flag: '', region: 'Europa' },
  { name: 'Russia', area: 2111, population: 15345, flag: '', region: 'Asia' },
  { name: 'Peru', area: 11, population: 15, flag: '', region: 'Americas' }
];

describe('TableComponent', () => {
  test('snapshot renders', () => {
    const component = renderer.create(
      <Table
        countries
        data={testData}
        averagePopulation={10}
        smallestBiggestCountry={smallestBiggestCountry}
      />
    );
    let newTable = component.toJSON();
    expect(newTable).toMatchSnapshot();
  });
});
