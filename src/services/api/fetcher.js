import axios from 'axios';

const apiURL =
  'https://restcountries.eu/rest/v2/all?fields=name;region;area;population;languages;flag';

const createProperDataList = apiResult => {
  const languageObj = {};
  let sumOfPopulation = 0;
  const smallestBiggestCountry = {
    biggest: {
      country: '',
      area: 2000
    },
    smallest: {
      country: '',
      area: 10000
    }
  };
  const countriesData = apiResult.data.map(country => {
    country.languages.forEach(language => {
      if (languageObj.hasOwnProperty(language.name)) {
        languageObj[language.name].countries.push(country.name);
        languageObj[language.name].populationTotal += country.population;
      } else {
        languageObj[language.name] = {
          countries: [country.name],
          populationTotal: country.population
        };
      }
    });
    sumOfPopulation += country.population || 0;
    if (country.area && country.area > smallestBiggestCountry.biggest.area) {
      smallestBiggestCountry.biggest.area = country.area;
      smallestBiggestCountry.biggest.country = country.name;
    } else if (
      country.area &&
      country.area < smallestBiggestCountry.smallest.area
    ) {
      smallestBiggestCountry.smallest.area = country.area;
      smallestBiggestCountry.smallest.country = country.name;
    }
    const population = country.population
      ? +(country.population / 1000000).toFixed(1)
      : 'N/A';
    const area = country.area ? Math.round(country.area / 2.59) : 'N/A';
    return {
      name: country.name,
      flag: country.flag,
      area,
      population,
      region: country.region || 'N/A'
    };
  });
  smallestBiggestCountry.biggest.area = Math.round(
    smallestBiggestCountry.biggest.area / 2.59
  );
  smallestBiggestCountry.smallest.area = +(
    smallestBiggestCountry.smallest.area / 2.59
  ).toFixed(3);
  return [
    countriesData,
    Object.keys(languageObj).map(key => ({
      language: key,
      populationTotal: languageObj[key].populationTotal,
      countries: languageObj[key].countries
    })),
    +(sumOfPopulation / apiResult.data.length / 1000000).toFixed(1),
    smallestBiggestCountry
  ];
};

const fetchAndPrepareData = () =>
  new Promise((resolve, reject) => {
    axios
      .get(apiURL)
      .then(result => resolve(createProperDataList(result)))
      .catch(error => reject(error));
  });

export default fetchAndPrepareData;
