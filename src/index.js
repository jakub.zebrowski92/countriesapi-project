import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App.jsx';
import './style/style.scss';

ReactDOM.render(<App />, document.getElementById('reactApp'));
